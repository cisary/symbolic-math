//
//  AppDelegate.h
//  Symbolic Math
//
//  Created by Michal Cisarik on 3/6/15.
//  Copyright (c) 2015 cisary solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

